#ifndef GAME_H_
#define GAME_H_

#include "player.h"
#include <utility>
#include <map>
#include <chrono>
#include <random>

class game {
	private:
		std::vector<player> players;
		int current_player;
		int state;
		std::mt19937_64 rng;
		uint64_t aleatorio(uint64_t a, uint64_t b);
	public:
		int last_dices_roll[2];
		std::vector<std::pair<int, int>> board;
		std::vector<property> properties;
		game();
		void addPlayer(const player & who);
		void start();
		int getState() const;
		int getCurrentPlayer() const;
		int numberOfPlayers() const;
		player& getPlayer(int idx);
		void makeMove();
		bool buyPropertyToCurrentPlayer(int idProperty);
		bool buildHouseToCurrentPlayer(int idProperty);
		void pass();
		bool payRent(int idProperty);
		bool payTax(int ammount);
		void addMoney(int ammount);
		void finishTurn();
};

#endif
