#include "game.h"

game::game() {
	properties.emplace_back(0, "Mediterranean Avenue", 60, 50, "#8B4513", std::vector<int>{2, 10, 30, 90, 160, 250});
	properties.emplace_back(1, "Baltic Avenue", 60, 50, "#8B4513", std::vector<int>{4, 20, 60, 180, 320, 450});
	properties.emplace_back(2, "Reading Railroad", 200, 50, "#000000", std::vector<int>{25, 50, 100, 200, 300, 400});
	properties.emplace_back(3, "Oriental Avenue", 100, 50, "#87CEEB", std::vector<int>{6, 30, 90, 270, 400, 550});
	properties.emplace_back(4, "Vermont Avenue", 100, 50, "#87CEEB", std::vector<int>{6, 30, 90, 270, 400, 550});
	properties.emplace_back(5, "Connecticut Avenue", 120, 50, "#87CEEB", std::vector<int>{8, 40, 100, 300, 450, 600});
	properties.emplace_back(6, "St. Charles Place", 140, 100, "#FF0080", std::vector<int>{10, 50, 150, 450, 625, 750});
	properties.emplace_back(7, "Electric Company", 150, 100, "#000000", std::vector<int>{5, 10, 15, 20, 25, 30});
	properties.emplace_back(8, "States Avenue", 140, 100, "#FF0080", std::vector<int>{10, 50, 150, 450, 625, 750});
	properties.emplace_back(9, "Virginia Avenue", 160, 100, "#FF0080", std::vector<int>{12, 60, 180, 500, 700, 900});
	properties.emplace_back(10, "Pennsylvania Railroad", 200, 50, "#000000", std::vector<int>{25, 50, 100, 200, 300, 400});
	properties.emplace_back(11, "St. James Place", 180, 100, "#FFA500", std::vector<int>{14, 70, 200, 550, 750, 950});
	properties.emplace_back(12, "Tennessee Avenue", 180, 100, "#FFA500", std::vector<int>{14, 70, 200, 550, 750, 950});
	properties.emplace_back(13, "New York Avenue", 200, 100, "#FFA500", std::vector<int>{16, 80, 220, 600, 800, 1000});
	properties.emplace_back(14, "Kentucky Avenue", 220, 150, "#FF0000", std::vector<int>{18, 90, 250, 700, 875, 1050});
	properties.emplace_back(15, "Indiana Avenue", 220, 150, "#FF0000", std::vector<int>{18, 90, 250, 700, 875, 1050});
	properties.emplace_back(16, "Illinois Avenue", 240, 150, "#FF0000", std::vector<int>{20, 100, 300, 750, 925, 1100});
	properties.emplace_back(17, "B&O Railroad", 200, 50, "#000000", std::vector<int>{25, 50, 100, 200, 300, 400});
	properties.emplace_back(18, "Atlantic Avenue", 260, 150, "#FFFF00", std::vector<int>{22, 110, 330, 800, 975, 1150});
	properties.emplace_back(19, "Ventnor Avenue", 260, 150, "#FFFF00", std::vector<int>{22, 110, 330, 800, 975, 1150});
	properties.emplace_back(20, "Marvin Gardens", 280, 150, "#FFFF00", std::vector<int>{24, 120, 360, 850, 1025, 1200});
	properties.emplace_back(21, "Pacific Avenue", 300, 200, "#008000", std::vector<int>{26, 130, 390, 900, 110, 1275});
	properties.emplace_back(22, "North Carolina Avenue", 300, 200, "#008000", std::vector<int>{26, 130, 390, 900, 110, 1275});
	properties.emplace_back(23, "Pennsylvania Avenue", 320, 200, "#008000", std::vector<int>{28, 150, 450, 1000, 1200, 1400});
	properties.emplace_back(24, "Short Line", 200, 50, "#000000", std::vector<int>{25, 50, 100, 200, 300, 400});
	properties.emplace_back(25, "Park Place", 350, 200, "#0000FF", std::vector<int>{35, 175, 500, 1100, 1300, 1500});
	properties.emplace_back(26, "Boardwalk", 400, 200, "#0000FF", std::vector<int>{50, 200, 600, 1400, 1700, 2000});
	board.emplace_back(-1, 0);  //#0:  GO
	board.emplace_back(0, 0);   //#1:  Property
	board.emplace_back(1, 50);  //#2:  Gain $50
	board.emplace_back(0, 1);   //#3:  Property
	board.emplace_back(2, 200); //#4:  Tax $200
	board.emplace_back(0, 2);   //#5:  Property
	board.emplace_back(0, 3);   //#6:  Property
	board.emplace_back(1, 75);  //#7:  Gain $75
	board.emplace_back(0, 4);   //#8:  Property
	board.emplace_back(0, 5);   //#9:  Property
	board.emplace_back(-1, 0);  //#10: Just visiting
	board.emplace_back(0, 6);   //#11: Property
	board.emplace_back(0, 7);   //#12: Property
	board.emplace_back(0, 8);   //#13: Property
	board.emplace_back(0, 9);   //#14: Property
	board.emplace_back(0, 10);  //#15: Property
	board.emplace_back(0, 11);  //#16: Property
	board.emplace_back(1, 100); //#17: Gain $100
	board.emplace_back(0, 12);  //#18: Property
	board.emplace_back(0, 13);  //#19: Property
	board.emplace_back(-1, 0);  //#20: Just visiting
	board.emplace_back(0, 14);  //#21: Property
	board.emplace_back(2, 175); //#22: Tax $175
	board.emplace_back(0, 15);  //#23: Property
	board.emplace_back(0, 16);  //#24: Property
	board.emplace_back(0, 17);  //#25: Property
	board.emplace_back(0, 18);  //#26: Property
	board.emplace_back(0, 19);  //#27: Property
	board.emplace_back(2, 150); //#28: Tax $150
	board.emplace_back(0, 20);  //#29: Property
	board.emplace_back(-1, 0);  //#30: Just visiting
	board.emplace_back(0, 21);  //#31: Property
	board.emplace_back(0, 22);  //#32: Property
	board.emplace_back(1, 125); //#33: Gain $125
	board.emplace_back(0, 23);  //#34: Property
	board.emplace_back(0, 24);  //#35: Property
	board.emplace_back(1, 150); //#36: Gain $150
	board.emplace_back(0, 25);  //#37: Property
	board.emplace_back(2, 100); //#38: Tax $100
	board.emplace_back(0, 26);  //#39: Property
	rng = std::mt19937_64(std::chrono::steady_clock::now().time_since_epoch().count());
}

uint64_t game::aleatorio(uint64_t a, uint64_t b) {
	std::uniform_int_distribution<uint64_t> dist(a, b);
	return dist(rng);
}

void game::addPlayer(const player & who) {
	players.push_back(who);
}

void game::start() {
	state = 0;
	current_player = 0;
	last_dices_roll[0] = last_dices_roll[1] = 0;
}

int game::getState() const{
	return state;
}

int game::getCurrentPlayer() const {
	return current_player;
}

int game::numberOfPlayers() const {
	return players.size();
}

player& game::getPlayer(int idx) {
	return players.at(idx);
}

void game::makeMove() {
	assert(state == 0);
	int a = aleatorio(1, 6);
	//int b = aleatorio(1, 6);
	players.at(current_player).advance(a );
	state = 1;
	last_dices_roll[0] = a;
	//last_dices_roll[1] = b;
}

bool game::buyPropertyToCurrentPlayer(int idProperty) {
	assert(state == 1);
	if (players.at(current_player).buyProperty(properties.at(idProperty))) {
		state = 2;
		return true;
	} else {
		return false;
	}
}

bool game::buildHouseToCurrentPlayer(int idProperty) {
	assert(state == 1);
	if (players.at(current_player).buildHouse(properties.at(idProperty))) {
		state = 2;
		return true;
	} else {
		return false;
	}
}

void game::pass() {
	assert(state == 1);
	state = 2;
}

bool game::payRent(int idProperty) {
	assert(state == 1);
	if (players.at(current_player).payRent(properties.at(idProperty).getCurrentRent(), players.at(properties.at(idProperty).getOwner()))) {
		state = 2;
		return true;
	} else {
		state = 3;
		return false;
	}
}

bool game::payTax(int ammount) {
	assert(state == 1);
	if (players.at(current_player).payTax(ammount)) {
		state = 2;
		return true;
	} else {
		state = 3;
		return false;
	}
}

void game::addMoney(int ammount) {
	assert(state == 1);
	players.at(current_player).addMoney(ammount);
	state = 2;
}

void game::finishTurn() {
	assert(state == 2 || state == 3);
	if (state == 3) {
		for (property & p : properties) {
			if (p.getOwner() == current_player) {
				p.reset();
			}
		}
		players.erase(players.begin() + current_player);
		current_player %= players.size();
		if (players.size() == 1) {
			state = 4;
		} else {
			state = 0;
		}
	} else {
		current_player = (current_player + 1) % players.size();
		state = 0;
	}
}
