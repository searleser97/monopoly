#include "mongoose.h"
#include <bits/stdc++.h>
#include "game.h"
using namespace std;

static const char *s_http_port = "8000";
static struct mg_serve_http_opts s_http_server_opts;
struct mg_mgr mgr;

void send(struct mg_connection *nc, const string & s){
	mg_printf(nc, "%s", "HTTP/1.1 200 OK\r\n"
		"Access-Control-Allow-Origin: *\r\n"
		"Content-Type: application/json\r\n"
		"Transfer-Encoding: chunked\r\n\r\n");
	mg_printf_http_chunk(nc, s.c_str());
	mg_send_http_chunk(nc, "", 0);
}

mt19937_64 rng(chrono::steady_clock::now().time_since_epoch().count());
const int64_t inf = numeric_limits<int>::max();

int64_t aleatorio(int64_t a, int64_t b) {
	uniform_int_distribution<int64_t> dist(a, b);
	return dist(rng);
}

regex requestPattern("(GET|POST|PUT|HEAD|DELETE)(\\s+)(\\/.*)?(\\s+)(HTTP\\/\\d\\.\\d)");

vector<string> split(const string & s, char delim = ' '){
	vector<string> ans;
	size_t i = s.find(delim), prev = 0;
	while(i != string::npos){
		ans.push_back(s.substr(prev, i - prev));
		prev = i + 1;
		i = s.find(delim, prev);
	}
	ans.push_back(s.substr(prev, i - prev));
	return ans;
}

string urldecode(const string & s){
	string ret;
	char ch;
	int ii;
	for(size_t i = 0; i < s.size(); ++i){
		if(s[i] == '%'){
			sscanf(s.substr(i+1, 2).c_str(), "%x", &ii);
			ch = static_cast<char>(ii);
			ret += ch;
			i = i + 2;
		}else{
			ret += s[i];
		}
	}
	return ret;
}

map<string, vector<string>> parseQuery(const string & s){
	map<string, vector<string>> ans;
	if(s == "") return ans;
	vector<string> fields = split(s, '&');
	for(const string & field : fields){
		vector<string> parts = split(field, '=');
		string key = parts[0];
		string value = parts.size() >= 2 ? parts[1] : "";
		ans[urldecode(key)].push_back(urldecode(value));
	}
	return ans;
}

map<int64_t, game> games;

static void ev_handler(struct mg_connection *nc, int ev, void *ev_data) {
	struct http_message *hm = (struct http_message *) ev_data;
	if (ev == MG_EV_HTTP_REQUEST) {
		string request(hm->message.p);
		request = request.substr(0, request.find("\r\n"));
		smatch matcher;
		if(!regex_match(request, matcher, requestPattern)){
			mg_serve_http(nc, hm, s_http_server_opts);
		}else{
			string method = matcher[1];
			string url = matcher[3];
			vector<string> parts = split(url, '?');
			string requestPath = parts[0];
			string queryString = parts.size() >= 2 ? parts[1] : "";

			map<string, vector<string>> getMap = parseQuery(queryString);

			if(requestPath == "/startNewGame"){
				int64_t id = aleatorio(0, inf);
				game g;
				for(const string & info : getMap["player"]){
					string name = split(info, ',')[0];
					int type = split(info, ',')[1][0] - '0';
					g.addPlayer(player(type, name, g.numberOfPlayers()));
				}
				g.start();
				games[id] = g;
				send(nc, "{\"id\": " + to_string(id) + "}");
			}else if(requestPath == "/getGamesStarted"){
				string ans = "{\"games\": [";
				for(auto it = games.begin(); it != games.end(); ++it){
					if(it != games.begin()) ans += ", ";
					ans += to_string(it->first);
				}
				ans += "]}";
				send(nc, ans);
			}else if(requestPath == "/getGameStatus"){
				int64_t id = stol(getMap["id"][0]);
				if(games.find(id) == games.end()){
					send(nc, "{\"status\": -1, \"error\": \"Game ID not found\"}");
				}else{
					game &g = games[id];
					string ans = "{\"status\": " + to_string(g.getState()) + ", \"currentPlayer\": " + to_string(g.getCurrentPlayer()) + ", \"players\": [";
					for(int k = 0; k < g.numberOfPlayers(); ++k) {
						player &p = g.getPlayer(k);
						if(k) ans += ", ";
						ans += "{\"id\": " + to_string(p.getID()) + ", \"type\": " + to_string(p.getType()) + ", \"name\": \"" + p.getName() + "\", \"position\": " + to_string(p.getPosition()) + ", \"money\": " + to_string(p.getMoney()) + "}";
					}
					ans += "], \"properties\": [";
					for(size_t k = 0; k < g.properties.size(); ++k) {
						property &p = g.properties.at(k);
						if(k) ans += ", ";
						ans += "{\"id\": " + to_string(p.getID()) + ", \"name\": \"" + p.getName() + "\", \"owner\": " + to_string(p.getOwner()) + ", \"color\": \"" + p.getColor() + "\", \"price\": " + to_string(p.getPrice()) + ", \"cost_house\": " + to_string(p.getCostHouse()) + ", \"houses\": " + to_string(p.getHouses()) + ", \"rent\": [";
						for(int l = 0; l < 6; ++l){
							if(l) ans += ", ";
							ans += to_string(p.getRent(l));
						}
						ans += "]}";
					}
					ans += "]";
					if(games[id].getState() == 1){
						ans += ", \"last_dices_roll\": [" + to_string(games[id].last_dices_roll[0]) +"]";
					}
					ans += ", \"error\": \"\"}";
					send(nc, ans);
				}
			}else if(requestPath == "/makeMove"){
				int64_t id = stol(getMap["id"][0]);
				if(games.find(id) == games.end()){
					send(nc, "{\"status\": -1, \"error\": \"Game ID not found\"}");
				}else{
					game &g = games[id];
					if(g.getState() != 0){
						send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"It is not the moment to make a new turn\"}");
					}else{
						g.makeMove();
						send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"\"}");
					}
				}
			}else if(requestPath == "/buyProperty"){
				int64_t id = stol(getMap["id"][0]);
				if(games.find(id) == games.end()){
					send(nc, "{\"status\": -1, \"error\": \"Game ID not found\"}");
				}else{
					game &g = games[id];
					if(g.getState() != 1){
						send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"It is not the moment to buy a property\"}");
					}else{
						int idProperty = stoi(getMap["idProperty"][0]);
						int pos = g.getPlayer(g.getCurrentPlayer()).getPosition();
						if(0 > idProperty || idProperty >= (int)g.properties.size() || g.board.at(pos).first != 0 || g.board.at(pos).second != idProperty || g.properties.at(idProperty).getOwner() != -1){
							send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"Invalid property ID\"}");
						}else{
							if(g.buyPropertyToCurrentPlayer(idProperty)){
								send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"\"}");
							}else{
								send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"The player doesn\"t have enough money to buy the property\"}");
							}
						}
					}
				}
			}else if(requestPath == "/buildHouse"){
				int64_t id = stol(getMap["id"][0]);
				if(games.find(id) == games.end()){
					send(nc, "{\"status\": -1, \"error\": \"Game ID not found\"}");
				}else{
					game &g = games[id];
					if(g.getState() != 1){
						send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"It is not the moment to build a house\"}");
					}else{
						int idProperty = stoi(getMap["idProperty"][0]);
						int pos = g.getPlayer(g.getCurrentPlayer()).getPosition();
						if(0 > idProperty || idProperty >= (int)g.properties.size() || g.board.at(pos).first != 0 || g.board.at(pos).second != idProperty || g.properties.at(idProperty).getOwner() == -1 || g.properties.at(idProperty).getOwner() != g.getCurrentPlayer()){
							send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"Invalid property ID\"}");
						}else{
							if(g.buildHouseToCurrentPlayer(idProperty)){
								send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"\"}");
							}else{
								send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"The player doesn\"t have enough money to build a house or already has 5 houses\"}");
							}
						}
					}
				}
			}else if(requestPath == "/pass"){
				int64_t id = stol(getMap["id"][0]);
				if(games.find(id) == games.end()){
					send(nc, "{\"status\": -1, \"error\": \"Game ID not found\"}");
				}else{
					game &g = games[id];
					if(g.getState() != 1){
						send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"It is not the moment to pass\"}");
					}else{
						int pos = g.getPlayer(g.getCurrentPlayer()).getPosition();
						bool cond1 = g.board.at(pos).first == 0 && g.properties.at(g.board.at(pos).second).getOwner() == -1;
						bool cond2 = g.board.at(pos).first == 0 && g.properties.at(g.board.at(pos).second).getOwner() == g.getCurrentPlayer();
						bool cond3 = g.board.at(pos).first == -1;
						if(!cond1 && !cond2 && !cond3){
							send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"Cannot pass\"}");
						}else{
							g.pass();
							send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"\"}");
						}
					}
				}
			}else if(requestPath == "/payRent"){
				int64_t id = stol(getMap["id"][0]);
				if(games.find(id) == games.end()){
					send(nc, "{\"status\": -1, \"error\": \"Game ID not found\"}");
				}else{
					game &g = games[id];
					if(g.getState() != 1){
						send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"It is not the moment to pay rent\"}");
					}else{
						int idProperty = stoi(getMap["idProperty"][0]);
						int pos = g.getPlayer(g.getCurrentPlayer()).getPosition();
						if(0 > idProperty || idProperty >= (int)g.properties.size() || g.board.at(pos).first != 0 || g.board.at(pos).second != idProperty || g.properties.at(idProperty).getOwner() == -1 || g.properties.at(idProperty).getOwner() == g.getCurrentPlayer()){
							send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"Invalid property ID\"}");
						}else{
							if(g.payRent(idProperty)){
								send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"\"}");
							}else{
								send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"The player is in bankrupcy\"}");
							}
						}
					}
				}
			}else if(requestPath == "/payTax") {
				int64_t id = stol(getMap["id"][0]);
				if(games.find(id) == games.end()){
					send(nc, "{\"status\": -1, \"error\": \"Game ID not found\"}");
				}else{
					game &g = games[id];
					if(g.getState() != 1){
						send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"It is not the moment to pay tax\"}");
					}else{
						int pos = g.getPlayer(g.getCurrentPlayer()).getPosition();
						if(g.board.at(pos).first != 2){
							send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"Invalid square to pay tax\"}");
						}else{
							if(g.payTax(g.board.at(pos).second)){
								send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"\"}");
							}else{
								send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"The player is in bankrupcy\"}");
							}
						}
					}
				}
			}else if(requestPath == "/gainMoney"){
				int64_t id = stol(getMap["id"][0]);
				if(games.find(id) == games.end()){
					send(nc, "{\"status\": -1, \"error\": \"Game ID not found\"}");
				}else{
					game &g = games[id];
					if(g.getState() != 1){
						send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"It is not the moment to add money\"}");
					}else{
						int pos = g.getPlayer(g.getCurrentPlayer()).getPosition();
						if(g.board.at(pos).first != 1){
							send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"Invalid square to gain money\"}");
						}else{
							g.addMoney(g.board.at(pos).second);
							send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"\"}");
						}
					}
				}
			}else if(requestPath == "/finishTurn"){
				int64_t id = stol(getMap["id"][0]);
				if(games.find(id) == games.end()){
					send(nc, "{\"status\": -1, \"error\": \"Game ID not found\"}");
				}else{
					game &g = games[id];
					if(g.getState() != 2 && g.getState() != 3){
						send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"It is not the moment to finish the turn\"}");
					}else{
						g.finishTurn();
						send(nc, "{\"status\": " + to_string(g.getState()) + ", \"error\": \"\"}");
					}
				}
			}else{
				mg_serve_http(nc, hm, s_http_server_opts);
			}
		}
	}
}

void isr(int sig){
	if(sig == SIGINT){
		mg_mgr_free(&mgr);
		printf("Stopping web server on port %s\n", s_http_port);
		exit(0);
	}
}

int main(void) {
	signal(SIGINT, isr);
	struct mg_connection *nc;
	mg_mgr_init(&mgr, NULL);
	printf("Starting web server on port %s\n", s_http_port);
	nc = mg_bind(&mgr, s_http_port, ev_handler);
	if (nc == NULL) {
		printf("Failed to create listener\n");
		return 1;
	}

	// Set up HTTP server parameters
	mg_set_protocol_http_websocket(nc);
	s_http_server_opts.document_root = "."; // Serve current directory
	s_http_server_opts.enable_directory_listing = "yes";
	for (;;) {
		mg_mgr_poll(&mgr, 1000);
	}

	return 0;
}
