#include "property.h"

property::property(): id(0), houses(0), price(0), cost_house(0), rent(6), owner(-1) {}

property::property(int id, const std::string & name, int price, int cost_house, const std::string & color, const std::vector<int> & rent): id(id), houses(0), price(price), cost_house(cost_house), rent(rent), color(color), name(name), owner(-1) {
	assert(rent.size() == 6);
}

bool property::addHouse() {
	if (houses == 5) {
		return false;
	} else {
		houses++;
		return true;
	}
}

std::string property::getName() const {
	return name;
}

int property::getID() const {
	return id;
}

int property::getPrice() const {
	return price;
}

int property::getCostHouse() const {
	return cost_house;
}

std::string property::getColor() const {
	return color;
}

int property::getRent(int idx) const {
	return rent.at(idx);
}

int property::getCurrentRent() const {
	return rent.at(houses);
}

int property::getHouses() const {
	return houses;
}

int property::getOwner() const {
	return owner;
}

void property::setOwner(int who) {
	owner = who;
}

void property::reset() {
	owner = -1;
	houses = 0;
}
