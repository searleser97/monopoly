#ifndef PROPERTY_H_
#define PROPERTY_H_

#include <vector>
#include <string>
#include <cassert>

class property {
	private:
		int id;
		int houses;
		int price;
		int cost_house;
		std::vector<int> rent;
		std::string color;
		std::string name;
		int owner;
	public:
		property();
		property(int id, const std::string & name, int price, int cost_house, const std::string & color, const std::vector<int> & rent);
		bool addHouse();
		std::string getName() const;
		int getID() const;
		int getPrice() const;
		int getCostHouse() const;
		std::string getColor() const;
		int getRent(int idx) const;
		int getCurrentRent() const;
		int getHouses() const;
		int getOwner() const;
		void setOwner(int who);
		void reset();
};

#endif
