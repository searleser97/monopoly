#ifndef PLAYER_H_
#define PLAYER_H_

#include "property.h"

class player {
	private:
		int type;
		std::string name;
		int money;
		int position;
		int id;
	public:
		player();
		player(int type, const std::string & name, int id);
		std::string getName() const;
		int getID() const;
		int getType() const;
		int getMoney() const;
		void addMoney(int inc);
		int getPosition() const;
		void advance(int inc);
		bool buyProperty(property & p);
		bool buildHouse(property & p);
		bool payRent(int ammount, player & who);
		bool payTax(int ammount);
};

#endif
