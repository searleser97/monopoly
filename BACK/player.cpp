#include "player.h"

player::player(): type(0), money(1500), position(0), id(0) {}

player::player(int type, const std::string & name, int id): type(type), name(name), money(1500), position(0), id(id) {}

std::string player::getName() const {
	return name;
}

int player::getID() const {
	return id;
}

int player::getType() const {
	return type;
}

int player::getMoney() const {
	return money;
}

void player::addMoney(int inc) {
	money += inc;
}

int player::getPosition() const {
	return position;
}

void player::advance(int inc) {
	int newPosition = position + inc;
	if (newPosition >= 40) {
		newPosition -= 40;
		money += 200;
	}
	position = newPosition;
}

bool player::buyProperty(property & p) {
	if (money >= p.getPrice()) {
		money -= p.getPrice();
		p.setOwner(id);
		return true;
	} else {
		return false;
	}
}

bool player::buildHouse(property & p) {
	if (p.getHouses() < 5 && money >= p.getCostHouse()) {
		money -= p.getCostHouse();
		p.addHouse();
		return true;
	} else {
		return false;
	}
}

bool player::payRent(int ammount, player & who) {
	if (money >= ammount) {
		money -= ammount;
		who.money += ammount;
		return true;
	} else {
		return false;
	}
}

bool player::payTax(int ammount) {
	if (money >= ammount) {
		money -= ammount;
		return true;
	} else {
		return false;
	}
}
